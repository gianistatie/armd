import torch
import torch.utils.data
from torchvision import datasets, transforms
from torch import nn, optim
from torch.nn import functional as F
from torchvision.utils import save_image

import matplotlib.pyplot as plt
from src.preparation.kermany_dataset import KermanyDataset
from src.preparation import kermany_transforms

class VAE(nn.Module):
    def __init__(self, img_size, dim_size):
        super(VAE, self).__init__()

        self.img_size = img_size

        self.fc1 = nn.Linear(img_size*img_size, 1024)
        self.fc11 = nn.Linear(1024, 512)
        self.fc12 = nn.Linear(512, 256)
        self.fc13 = nn.Linear(256, 128)
        self.fc21 = nn.Linear(128, dim_size)
        self.fc22 = nn.Linear(128, dim_size)

        self.fc3 = nn.Linear(dim_size, 128)
        self.fc31 = nn.Linear(128, 256)
        self.fc32 = nn.Linear(256, 512)
        self.fc33 = nn.Linear(512, 1024)
        self.fc4 = nn.Linear(1024, img_size*img_size)

    def encode(self, x):
        h1 = F.relu(self.fc1(x))
        h1 = F.relu(self.fc11(h1))
        h1 = F.relu(self.fc12(h1))
        h1 = F.relu(self.fc13(h1))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.rand_like(std)
        return mu + eps*std

    def decode(self, z):
        h3 = F.relu(self.fc3(z))
        h3 = F.relu(self.fc31(h3))
        h3 = F.relu(self.fc32(h3))
        h3 = F.relu(self.fc33(h3))
        return torch.sigmoid(self.fc4(h3))

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, self.img_size*self.img_size))
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

def loss_function(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x.view(recon_x.shape), reduction='sum')

    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE + KLD

def train(epoch, train_loader):
    model.train()
    train_loss = 0 
    for batch_idx, (data, _) in enumerate(train_loader):
        data = data.to(device)
        optimizer.zero_grad()
        recon_batch, mu, logvar = model(data)
        loss = loss_function(recon_batch, data, mu, logvar)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader),
                loss.item() / len(data)))

    print('====> Epoch: {} Average loss: {:.4f}'.format(
            epoch, train_loss / len(train_loader.dataset)))

def test(epoch, test_loader):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for i, (data, _) in enumerate(test_loader):
            data_shape = data.shape
            data = data.to(device)
            recon_batch, mu, logvar = model(data)
            test_loss += loss_function(recon_batch, data, mu, logvar).item()
            if i == 0:
                n = min(data.size(0), 8)
                comparison = torch.cat([data[:n],
                                      recon_batch.view(data_shape)[:n]])
                save_image(comparison.cpu(),
                         '../data/results/reconstruction_' + str(epoch) + '.png', nrow=n)

    test_loss /= len(test_loader.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))

if __name__ == "__main__":
    device = torch.device("cuda")
    kwargs = {'num_workers': 1, 'pin_memory': True}

    img_size = 256
    dim_size = 64

    dataset = KermanyDataset(r'D:\Proiecte\AI\armd\data\raw\02_oct\02_Kermany\test',
                        transform=transforms.Compose([kermany_transforms.FillWhites(),
                                                    transforms.Resize((img_size, img_size)),
                                                    transforms.Grayscale(num_output_channels=1),
                                                    transforms.ToTensor()]))

    plt.imshow(dataset[0][0][0])
    plt.show()

    train_loader = torch.utils.data.DataLoader(dataset,
        batch_size=128, shuffle=True, **kwargs)

    test_loader = torch.utils.data.DataLoader(dataset,
        batch_size=128, shuffle=True, **kwargs)

    model = VAE(img_size=img_size, dim_size=dim_size).to(device)
    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    for epoch in range(1, 100 + 1):
        train(epoch, train_loader)
        test(epoch, test_loader)
        with torch.no_grad():
            sample = torch.randn(64, dim_size).to(device)
            sample = model.decode(sample).cpu()
            save_image(sample.view(64, 1, img_size, img_size),
                       '../data/results/sample_' + str(epoch) + '.png')