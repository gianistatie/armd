import cv2
import numpy as np

from PIL import Image

class FillWhites(object):
    def __call__(self, sample):
        image_array = np.asarray(sample)

        ## (1) Convert to gray, and threshold
        th, threshed = cv2.threshold(image_array, 240, 255, cv2.THRESH_BINARY_INV)

        ## (2) Morph-op to remove noise
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (13,13))
        morphed = cv2.morphologyEx(threshed, cv2.MORPH_CLOSE, kernel)

        ## (3) Fill in the outer white spaces of the image with black
        mask = cv2.bitwise_not(morphed)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
        mask = cv2.dilate(mask,kernel,iterations = 2)

        filled_scan = cv2.subtract(image_array, mask)
        filled_scan = np.clip(filled_scan, 0, 255)

        return Image.fromarray(filled_scan)