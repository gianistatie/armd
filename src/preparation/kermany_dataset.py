from PIL import Image
import torch
import numpy as np

from pathlib import Path
from torch.utils.data import Dataset

class KermanyDataset(Dataset):

    def __init__(self, root_dir, transform=None):

        self.root_dir = Path(root_dir)
        self.transform = transform
        self.image_paths = sorted(list(self.root_dir.glob('*/*')))

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, idx):
        scan = Image.open(self.image_paths[idx])
        
        affection = np.array([0,0,0,0])

        # TODO retrun affection (DME, Normal, Drusen etc) as one-hot

        if self.transform:
            scan = self.transform(scan)

        return scan, affection

        